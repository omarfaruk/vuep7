import { createApp } from 'vue'
import App from './App.vue'
import fContact from './components/friendContact.vue'

const app = createApp(App);
app.component('fnd-contact',fContact);
app.mount('#app');
